# Buy Me a Tea

The purpose of this project is merely to accomplish a technical assignment. It mimics the donation and subscription functionality found at https://www.buymeacoffee.com/ but with the difference that it will make use of Adyen instead of Stripe.
